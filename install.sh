#!/bin/bash

set -x
export DEBIAN_FRONTEND=noninteractiv
sudo apt-get update -y
sudo apt-get install -y \
  zsh git vim \
  awscli \
  python3.9 python3.9-dev python3-pip

python3.9 -m pip install -U pi
python3.9 -m pip install ansible poetry
