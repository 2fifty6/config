#!/bin/bash -ex

netdevice=e1000e
driverdir=/media/exbyte/ReFoRMaT/Linux/drivers/$netdevice-3.8.4
# if [[ ! -d $driverdir ]]; then
#   wget https://downloadmirror.intel.com/15817/eng/e1000e-3.8.4.tar.gz
#   tar xzf
# fi
cd $driverdir/src
sudo make install

set +e
sudo modinfo $netdevice >/dev/null 2>&1 && sudo rmmod $netdevice
set -e
sudo modprobe $netdevice

sudo update-initramfs -u
