#!/bin/bash -xe
winetricks -q \
  win10

# configure wine
winetricks -q \
  cabinet \
  corefonts \
  dxvk \
  urlmon \
  winhttp \
  wininet

