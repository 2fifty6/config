#!/bin/bash -xe

sudo dpkg --add-architecture i386
export WINEPREFIX=~/.wine

# Update software sources
sudo apt-get update -y
sudo apt-get install -y \
  apt-transport-https \
  carla \
  carla-bridge-win32 \
  carla-bridge-win64 \
  dosbox \
  exe-thumbnailer \
  fluidsynth \
  gpgv \
  gstreamer1.0-libav \
  gstreamer1.0-libav:i386 \
  gstreamer1.0-plugins-bad \
  gstreamer1.0-plugins-bad:i386 \
  gstreamer1.0-plugins-ugly \
  gstreamer1.0-plugins-ugly:i386 \
  jackd2 \
  kio-extras \
  playonlinux \
  qjackctl \
  software-properties-common \
  ttf-mscorefonts-installer \
  ttf-mscorefonts-installer:i386 \
  wget \
  winbind

wget -qO- https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -
sudo apt-add-repository -y "deb https://dl.winehq.org/wine-builds/ubuntu/ $(lsb_release -cs) main"
sudo apt update -y
sudo apt install --install-recommends winehq-devel winetricks
wine --version

./scripts/winetricks.sh
./scripts/wineasio.sh
