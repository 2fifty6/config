#!/bin/bash -x

pushd `dirname $0` >/dev/null
CURRDIR=`pwd`

# config files
ln -sf $CURRDIR/.lfntrc ~/.lfntrc
ln -sf $CURRDIR/files/gitconfig ~/.gitconfig
# ln -sf $CURRDIR/files/jackdrc ~/.jackdrc
ln -sf $CURRDIR/files/settings.json ~/.vscode/settings.json
ln -sf $CURRDIR/files/vimrc ~/.vimrc
ln -sf $CURRDIR/files/zprompt ~/.zprompt
ln -sf $CURRDIR/files/zshrc ~/.zshrc

# dotfiles
mkdir -p ~/.dotfiles 2>/dev/null
ln -sfn $CURRDIR/files/initscripts ~/.dotfiles/initscripts
ln -sfn $CURRDIR/scripts ~/scripts

# vim
[[ ! -e ~/.vim/bundle/Vundle.vim ]] && git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qa

# packages
if [[ ! -z "`which brew 2>/dev/null`" ]]; then
  brew install direnv autojump thefuck jq go pulumi awscli wget python3
else
  ansible localhost -clocal -i/dev/null \
    -m ansible.builtin.package \
    -a'name=direnv,autojump,thefuck' -b
fi
