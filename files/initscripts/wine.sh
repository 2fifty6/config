#!/bin/bash
alias wineedit="vim $0"
alias winerefresh="source $0"

export DEFAULT_WINE_PREFIX=/home/dswartz/.wine

#export WINEPREFIX=~/.local/share/wineprefixes/winemedia/
#WINEARCH=win64 winecfg
export MESA_GL_VERSION_OVERRIDE=4.5
export GR6_ENABLE_FILE_LOGGING=True

export WINE_PROGRAM_FILES="$DEFAULT_WINE_PREFIX/drive_c/Program\ Files"
alias native-access="wine $WINE_PROGRAM_FILES/Native\ Instruments/Native\ Access/Native\ Access.exe"
alias guitar-rig="wine $WINE_PROGRAM_FILES/Native\ Instruments/Guitar\ Rig\ 6/Guitar\ Rig\ 6.exe"
alias studio-one="wine $WINE_PROGRAM_FILES/PreSonus/Studio\ One\ 5/Studio\ One.exe"

function fluid(){
  ps aux | grep fluid | grep -v grep >/dev/null || systemctl --user start fluidsynth
}
function guitar(){
  fluid
  qjackctl&
  guitar-rig&
}
