alias brewedit="vim $0"
alias brewrefresh="source $0"

[[ -d /home/linuxbrew/.linuxbrew/bin ]] && export PATH=$PATH:/home/linuxbrew/.linuxbrew/bin
[[ -d /home/linuxbrew/.linuxbrew/opt/python@3.10/bin ]] && export PATH="/home/linuxbrew/.linuxbrew/opt/python@3.10/bin:$PATH"

# to fix brew python
# sudo update-alternatives --install /usr/bin/python3 python /home/linuxbrew/.linuxbrew/opt/python@3.10/bin/python3.10 1
