# task.sh

alias taskedit="vim $0"
alias tedit=taskedit
alias taskrefresh="source $0"
alias trefresh=taskrefresh

alias t="task"
alias tl="task -l"

alias tcomp="complete -W \"\`task -l | grep '^*' | sed 's/: //g' | awk '{print \$2}'\`\" task"
