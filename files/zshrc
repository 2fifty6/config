# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
source $ZSH/oh-my-zsh.sh
eval "$(/opt/homebrew/bin/brew shellenv)"


# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"
CORRECT_IGNORE="..."
# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  aws
  autojump
  brew
  direnv
  docker
  git
  poetry
)

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

for script in `ls ~/.dotfiles/initscripts`; do source ~/.dotfiles/initscripts/$script; done

# OS X
if [[ `uname` == "Darwin" ]]; then
  alias ls='ls -G '
  alias grep='grep --color=always'
  alias showhidden="defaults write com.apple.Finder AppleShowAllFiles YES && killall Finder"
  alias hidehidden="defaults write com.apple.Finder AppleShowAllFiles NO && killall Finder"
  # homebrew
  which brew >/dev/null 2>&1 || ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  which wget >/dev/null 2>&1 || brew install wget
  # Various Plugins
  [[ ! -e /usr/local/share/zsh/site-functions/_aws ]] && (brew install awscli >/dev/null 2>&1 || brew link --overwrite awscli)
  . /usr/local/share/zsh/site-functions/_aws 2>/dev/null
fi

# Ctrl-S is a pain in the ass
stty -ixon

# CONVENIENCE
alias ltr='ls -ltr'
function grepv () { grep "$@" | grep -v --color grep }
function psgrep () { ps aux | grep "$@" | grep -v --color grep }
function fromnow (){ date -v+$1 "+%Y-%m-%d" }
function suniq(){ sort | uniq <&3 }
alias vedit='vim ~/.vimrc'

# output formatting
BOLD="\e[1m"
ITALIC="\e[3m"
PLAIN="\e[0m"
GREEN="\e[32m"
RED_BOLD="\e[01;31m"
function shdo () {
  FULLCMD=($*)
  CMD="$1" ARGS=${FULLCMD[@]:1}
  echo -en "\e[01m* $CMD $ARGS\e[0m"
  NUMTABS=$((`tput cols`/8 - 2))
  echo -ne "\r"
  for X in {1..$NUMTABS}
  do
    echo -ne "\t"
  done
  echo -e "    \e[0;1m[ \e[32m`date +%T` \e[0;1m]\e[0m"
  eval $CMD $ARGS
  RC=$?
  if [ $RC -eq 0 ]
  then
    echo -e "[ ${GREEN}OK${PLAIN} ]"
  else
    echo -e "[ ${RED_BOLD}FAIL${PLAIN} ] ($RC)"
  fi
}

source ~/.zprompt
setprompt

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
autoload bashcompinit && bashcompinit
autoload -Uz compinit && compinit
eval "$(direnv hook zsh)"
complete -C "`which aws_completer`" aws
eval "$(_LFNT_COMPLETE=zsh_source lfnt 2>/dev/null)"

export PATH="$HOME/.poetry/bin:$PATH:/usr/local/bin"

# rustup
source "$HOME/.cargo/env"
